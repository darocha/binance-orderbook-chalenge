import React from "react";
import "./App.css";
import { Header } from "./components/Header";
import { Routes, Route, BrowserRouter } from "react-router-dom";
import TraderWindow from "./components/TraderWindow";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Header />
        <Routes>
          <Route path="/" element={<TraderWindow />} />
          <Route path="/:pair" element={<TraderWindow />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
