import { useEffect, useState } from "react";
import { Tab } from "../../types";

export default function useTabs() {
  const [tabs, setTabs] = useState<Tab[]>([]);

  const addTab = (tab: Tab) => {
    if (!tabs.find((t) => t.id === tab.id)) {
      setTabs([...tabs, tab]);
    }
  };

  const removeTab = (tab: Tab) => {
    const index = tabs.findIndex((t) => t.id === tab.id);
    console.log('index', index)
    tabs.splice(index, 1);
    console.log('tabs', tabs)
    setTabs([...tabs]);
  };

  const loadTabs = () => {
    const tabsValue = localStorage.getItem("tabs");
    if (tabsValue) {
      setTabs(JSON.parse(tabsValue));
    }
  };

  useEffect(() => {
    loadTabs();
  }, []);

  return { addTab, removeTab, tabs };
}
