import {
  useCallback,
  useEffect,
  useMemo,
  useReducer,
  useState,
  useRef,
} from "react";
import logger from "../../lib/logger";
import SocketClient from "../../lib/socketClient";
import { useCoinPair, useTabs } from "..";

export const CONNECTION_STATE = [
  "Connecting", // CONNECTING
  "Connected", // OPEN
  "Closing", // CLOSING
  "Disconnected", // CLOSED
];

function orderBookReducer(state: any, action: any) {
  switch (action.type) {
    case "UPDATE":
      return {
        ...state,
        [action.pair]: action.data,
      };
    default:
      return state;
  }
}

export default function useWebSocket() {
  const depth = "@depth10";
  const [connectionState, setConnectionState] = useState(CONNECTION_STATE[0]);
  const [orderBookData, dispatch] = useReducer(orderBookReducer, {});
  const { pair } = useCoinPair();
  const { tabs } = useTabs();
  const socket2Api: any = useRef();

  // stream the current selected pair and all pair tabs
  const pairs = useMemo(() => {
      return [
        `${pair?.id.toLowerCase()}${depth}`,
        ...(tabs?.map((pair) => `${pair?.id.toLowerCase()}${depth}`) || []),
      ];
  },[pair?.id, tabs])

  const pairString = pairs?.join("/");
  logger.info(pairString);

  const updateState = useCallback(() => {
    const state = socket2Api.current?._ws?.readyState ?? 0;
    setConnectionState(CONNECTION_STATE[state]);
  }, [setConnectionState]);

  const onopen = useCallback(() => {
    logger.warn("ws open");
    updateState();
  }, [updateState]);

  const onerror = useCallback(
    (err: any) => {
      logger.warn("ws error", err);
      updateState();
    },
    [updateState]
  );

  const onclose = useCallback(() => {
    logger.warn("ws closed");
    updateState();
  }, [updateState]);

  const options = useMemo(() => {
    return {
      onopen,
      onclose,
      onerror,
    };
  }, [onopen, onclose, onerror]);

  const updateData = (params: any) => {
    logger.info(params);

    try {
      const pair = params.stream?.split("@")[0].toUpperCase();
      const data = params.data;
      dispatch({ type: "UPDATE", pair, data });
    } catch (error) {
      console.log("socket error", error);
    }
  };

  const isConnected = () => connectionState === CONNECTION_STATE[1];

  const addHandlers = useCallback(() =>{
    pairs?.forEach((pair) => {
      socket2Api.current.setHandler(pair, (params: any) => updateData(params));
    });
  }, [pairs])

  useEffect(() => {
    console.log("pairs changed", pair);
    socket2Api.current?.destroy();
    socket2Api.current = new SocketClient(
      `stream?streams=${pairString}`,
      options
    );
    addHandlers();
  }, [pair, options, pairString, socket2Api, addHandlers]);

  return { orderBookData, connectionState, isConnected };
}
