import { createContext, ReactNode, useContext, useEffect, useState } from 'react';
import pairsData from "../../data/pairs.json";
import { Pair } from '../../types';
import { config } from "../../config";
import { useParams } from "react-router-dom";

declare type CoinPairContext = {
    pair: null | Pair,
    pairs: null | Pair[],
    changePair: any | Function
}

const Context = createContext<CoinPairContext>({
    pair: null,
    pairs: [],
    changePair: () => {}
});

export default function useCoinPair() {
  return useContext(Context);
}

export const CoinPairProvider = ({ children } : { children: ReactNode }) => {
    
    const urlParams = useParams();
    const [pair, setPair] = useState<null|Pair>(config.defaultPair)
    const [pairs, setPairs] = useState<Pair[]>([])

    const changePair = (value: Pair) => {
        setPair(value);
    }
    
    const loadPair = () => {
        let pairValue: string | Pair | null = localStorage.getItem('pair');
        if (pairValue){
            pairValue = JSON.parse(pairValue);
            setPair(pairValue as Pair)
        }
        
        const urlPair = urlParams.pair;
        // if there is a pair param override current pair;
        if(urlPair && urlPair !== pair?.id){
            const p:Pair | undefined = pairs?.find(p => p.id === urlPair);
            if (p){
               setPair(p as Pair)
            }
        }
    }

    const fetchPairs = () => {
        // todo: fetch pairs from api
        setPairs(pairsData);
    }
   
    useEffect(() => {
        fetchPairs();
        loadPair();
    }, [])
  
    const value = {
        changePair,
        pair,
        pairs
    }

    return (
        <Context.Provider value={value}>
            {children}
        </Context.Provider>
    );
};

