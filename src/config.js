export const config = {
    defaultPair: {
        label: "BTC/USDT",
        id: "BTCUSDT"
    },
    binance: {
        endpoints:{
            wssBase: "wss://stream.binance.com:9443/ws/",
            wss: "wss://stream.binance.com:9443/ws/bnbusdt@depth20@5000ms"
        }
    }
}
