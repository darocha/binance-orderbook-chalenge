import * as React from 'react';
import TextField from '@mui/material/TextField';
import Autocomplete from '@mui/material/Autocomplete';
import parse from 'autosuggest-highlight/parse';
import match from 'autosuggest-highlight/match';
import { AutocompleteItem } from "../../types";
import { makeStyles } from '@mui/styles';

const useStyles = makeStyles({
  autocomplete: {
    display: 'inline-flex', 
    marginRight: '10px'
  }
});

/**
 * 
 * @param pairs a list of coin pairs ex: [{ label: "BTC/USDT", ID: "BTCUSDT" }]
 * @param label a user friendly label for the input field ex: "Search Coin"
 * @returns Autocomplete Textfield component with a list items that can be searched/filtered by typing a value 
 */
export default function AutocompleteSelect({ items, label, value, onChange=()=>{} }: { items: AutocompleteItem[], label?: string, 
  value?: null | AutocompleteItem, onChange?: Function }) {

  const classes = useStyles();

  return (
    <Autocomplete
      sx={{ width: 200 }}
      className={classes.autocomplete}
      options={items}
      getOptionLabel={(option) => option.label}
      onChange={(e, newValue) => onChange(newValue)}
      value={value}
      renderInput={(params) => (
        <TextField {...params} label={label} margin="normal" />
      )}
      renderOption={(props, option, { inputValue }) => {
        const matches = match(option.label, inputValue);
        const parts = parse(option.label, matches);

        return (
          <li {...props}>
            <div>
              {parts.map((part, index: number) => (
                <span
                  key={index}
                  style={{
                    fontWeight: part.highlight ? 700 : 400,
                  }}
                >
                  {part.text}
                </span>
              ))}
            </div>
          </li>
        );
      }}
    />
  );
}
