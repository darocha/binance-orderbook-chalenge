import React, { useEffect, useRef } from "react";
import { makeStyles } from "@mui/styles";
import Big from 'big.js';
import useWebSocket from "../../hooks/useWebSocket";
import DynamicTable from "../DynamicTable";
import { useCoinPair } from "../../hooks";
import { config } from "../../config";
import { ColumnData } from "../DynamicTable";

const useStyles = makeStyles({
  table: {
    userSelect: 'none',
    background: 'transparent',
    color: '#fff',
    borderRadius: 0,
    '& td:nth-child(3)':{
      minWidth: 100,
    },
    '& th':{
      color: '#7791a5',
      borderBottom: '0',
      paddingBottom: 2
    },
    '& td':{
      color: '#ddd',
      borderBottom: 0,
      minWidth: 90,
      padding: '4px 16px'
    },
    '& td:last-child':{
      width: 'auto'
    },
    '& tbody tr:nth-child(11)':{
      borderTop: '5px solid #26282f !important',
      borderBottom: '5px solid #26282f !important'
    }
  },
  askColumn: {
    color: "red !important",
  },
  bidColumn: {
    color: "green !important",
  },
  centerRow: {
    color: "green !important",
    background: '#1d1f24',
    padding: "12px 16px !important",
    fontSize: '16px !important',
    '&:nth-child(2), &:nth-child(3)':{
      color: "#fff !important",
    }
  },
});

export default function OrderBook() {
  const classes = useStyles();
  const { orderBookData } = useWebSocket();
  const { pair } = useCoinPair();
  const oBookData = useRef<any[] | null>();
  const oBookHeader = useRef<any[] | null>();

  useEffect(() => {
    const data = orderBookData[pair?.id ?? config.defaultPair.id];

    const symbols = pair?.label?.split("/");
    const PriceSymbol = symbols?.[1];
    const AmountSymbol = symbols?.[0];

    oBookHeader.current = [
      {
        value: `Price (${PriceSymbol})`,
        props: { align: "left", className: "red" },
      },
      { value: `Amount (${AmountSymbol})`, props: { align: "right" } },
      { value: "Total", props: { align: "right" } },
    ];

    const bids = data?.bids?.map((row: any) => [
      { value: row[0], props: { align: "left", className: classes.bidColumn, isBid: true } },
      { value: row[1], props: { align: "right" } },
      { value: new Big(row[0]).mul(row[1]).toString(), props: { align: "right", decimalScale: 4 } },
    ]);

    const asks = data?.asks?.map((row: any) => [
      { value: row[0], props: { align: "left", className: classes.askColumn, isAsk: true } },
      { value: row[1], props: { align: "right" } },
      { value: new Big(row[0]).mul(row[1]).toString(), props: { align: "right", decimalScale: 4 } },
    ]);

    const centerRow = asks && asks[0].map((col:any)=> {return {...col, props: { ...col.props, className: classes.centerRow, isAsk: true }  }});

    oBookData.current = bids && asks ? [...asks.reverse(), centerRow , ...bids] : [];
  }, [orderBookData, oBookHeader, oBookData, pair, classes]);

  const onCellClick = (col: ColumnData, cellIndex:number) => {
    console.log(col.value, cellIndex, col.props)
  }

  return (
    <DynamicTable
      onCellClick={onCellClick}
      className={classes.table}
      headCols={oBookHeader.current as ColumnData[]}
      data={oBookData.current as [ColumnData[]]}
    />
  );
}
