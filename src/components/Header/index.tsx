import { NavLink, useNavigate } from "react-router-dom";
import { useCoinPair, useTabs } from "../../hooks";
import { Tab, Pair } from "../../types";
import CoinSelect from "../Autocomplete";
import { makeStyles } from "@mui/styles";
import ClearIcon from "@mui/icons-material/Clear";
import useWebSocket from "../../hooks/useWebSocket";
import React, { MouseEvent } from "react";

const useStyles = makeStyles({
  nav: {
    padding: "0px 15px 10px",
    display: "flex",
    justifyContent: "space-between",
    alignItems: "baseline",
    minHeight: 85,
    maxHeight: 85,
    overflow: "hidden",
  },
  navlink: {
    background: "#fff",
    border: "1px solid #333",
    borderRadius: 3,
    color: "#333",
    height: 48,
    padding: "5px 15px 5px 20px",
    margin: "0 5px",
    textDecoration: "none",
    display: "inline-flex",
    justifyContent: "space-between",
    alignItems: "center",
    boxSizing: "border-box",
    "&:hover": {
      background: "#444",
      color: "#fff",
    },
  },
  active: {
    background: "#333",
    border: 0,
    color: "white",
  },
  icon: {
    marginLeft: "10px",
  },
  connected: {
    color: "green",
    padding: "0 10px",
  },
  disconnected: {
    color: "red",
    padding: "0 10px",
  },
});

export function Header() {
  const { connectionState, isConnected } = useWebSocket();
  const classes = useStyles();
  const { tabs, addTab, removeTab } = useTabs();
  const { pair, pairs, changePair } = useCoinPair();
  const navigate = useNavigate();

  const onChange = (selectedPair: null | Pair) => {
    if (selectedPair) {
      changePair(selectedPair);
      addTab(selectedPair as Tab);
      navigate(`/${selectedPair.id}`);
    }
  };

  const onRemoveTabClick = (
    tabPair: Pair,
    event: React.MouseEvent<Element, globalThis.MouseEvent>
  ) => {
    event.preventDefault();
    event.stopPropagation();
    removeTab(tabPair);
  };

  return (
    <nav className={classes.nav}>
      <div>
        <CoinSelect
          onChange={onChange}
          value={pair}
          items={pairs as Pair[]}
          label="Search Coin"
        />
        {tabs.map((tab: Tab, i: number) => (
          <NavLink
            onClick={() => onChange(tab)}
            key={i}
            className={({ isActive }) =>
              isActive
                ? `${classes.navlink} ${classes.active}`
                : classes.navlink
            }
            to={`/${tab.id}`}
          >
            {tab.label}
            <ClearIcon
              onClick={(e: MouseEvent<Element>) => onRemoveTabClick(tab, e)}
              className={classes.icon}
            />
          </NavLink>
        ))}
      </div>
      <div className={isConnected() ? classes.connected : classes.disconnected}>
        {connectionState}
      </div>
    </nav>
  );
}
