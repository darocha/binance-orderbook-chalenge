
/**

  sample data structure

  type ColumnData
  
  table header:

  headCols = [
    { value: "Price (USDT)" },
    { value: "Amount (BTC)" },
    { value: "Total"}
  ];

  table header with props:

  headCols = [
    { value: "Price (USDT)", props: {align: "left", className:"red"} },
    { value: "Amount (BTC)", props: { align: "center" } },
    { value: "Total", props: { align: "right" } }
  ];

  rows: 

  data = [
    [
      { value: "38027.81", props: { align: "left" }},
      { value: "0.130000", props: { align: "center" }},
      { value: "38027.81", props: { align: "right" }}
    ],
  ]

*/
import * as React from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import NumberFormat from "react-number-format";

export declare type ColumnData = {
  value: any;
  props?: {
    align?: "inherit" | "left" | "right" | "center" | "justify" | undefined;
    [x: string]: any;
  };
};

export default function DynamicTable({
  headCols,
  data,
  className,
  onCellClick
}: {
  headCols?: ColumnData[];
  data?: [ColumnData[]] | [] | undefined;
  className?: string,
  onCellClick: Function
}) {

  const renderHeadCol = (value: any, props: any, index: number) => {
    return (
    <TableCell key={index} {...props}>
      {value}
    </TableCell>
  )};

  const renderBodyCol = (col: ColumnData, index: number) => (
    <TableCell key={index} {...col.props} onClick={() => onCellClick(col, index)}>
      <NumberFormat
        thousandSeparator=","
        decimalSeparator="."
        prefix=""
        suffix=""
        allowNegative={false}
        displayType="text"
        fixedDecimalScale={true}
        decimalScale={col.props?.decimalScale ?? 4}
        value={col.value}
      />
    </TableCell>
  );

  const renderRow = (row: any, index: number) => {
    return (
      <TableRow
        key={index}
        sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
      >
        {row.map((col: any, colIndex: number) =>
          renderBodyCol(col, colIndex)
        )}
      </TableRow>
    );
  };

  return (
    <TableContainer component={Paper} style={{borderRadius:0, background: 'transparent'}} >
      <Table className={className} sx={{ width: "100%" }} size="small" aria-label="dynamic table">
        <TableHead>
          <TableRow>
            {headCols?.map((col, colIndex) =>
              renderHeadCol(col.value, col.props, colIndex)
            )}
          </TableRow>
        </TableHead>
        <TableBody>
          {data?.map((row: any, rowIndex: number) => renderRow(row, rowIndex))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
