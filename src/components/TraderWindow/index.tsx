import OrderBook from "../OrderBook";
import { styled } from "@mui/material/styles";
import Paper from "@mui/material/Paper";
import Grid from "@mui/material/Grid";
import { makeStyles } from "@mui/styles";

const useStyles = makeStyles({
  traderWindow: {
    height: "100%",
    padding: "0 8px 8px",
    background: "#101117",
  },
});

const Item = styled(Paper)(({ theme }) => ({
  ...theme.typography.body2,
  textAlign: "center",
  color: theme.palette.text.secondary,
  height: "100%",
  display: "flex",
  borderRadius: 0,
  background: "#26282f",
}));

export default function Trader() {
  const classes = useStyles();

  return (
    <Grid
      className={classes.traderWindow}
      container
      spacing={1}
      alignItems="stretch"
    >
      <Grid item xs>
        <Item />
      </Grid>
      <Grid item xs={3}>
        <Item>
          <OrderBook />
        </Item>
      </Grid>
      <Grid item xs={3}>
        <Item></Item>
      </Grid>
    </Grid>
  );
}
