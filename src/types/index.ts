
export type Product = {
    name: string;
    calories: number;
    fat: number; 
    carbs: number;
    protein: number;
}

export type AutocompleteItem = {
    label: string;
    id: string;
}

export type Pair = AutocompleteItem;

export type Tab = {
    label: string;
    id: string;
};
